# README #

Kudo Voucher Go is API for handling voucher using Go Programming Language

### Kudo Voucher Go ###

* Kudo Voucher Go is API for handling voucher using Go Programming Language
* 0.1

### Setup ###

* Setup and Configuring Go SDK please refer to : https://golang.org/doc/install
* Installing Beego (Go) Web Framework
  
  `go get github.com/astaxie/beego`

* Dependencies
  a. Go SDK (recomendation using Go SDK 1.7)
  b. Beego (Go) Web Framework.

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact